package commandPattern;

import commandPattern.command.CommandHistory;
import commandPattern.command.CommandManager;
import commandPattern.command.PopCommand;
import commandPattern.command.PushCommand;

import java.awt.event.*;
import javax.swing.*;

/* ************************************
 *
 *  Lab4.
 *
 * ************************************/

 public class Lab4 extends JFrame implements Observer
{
    private VStack stack = new VStack();  // the stack object
    private String pushstring=""; // the string to push on the stack
    
	public Lab4()
	{
		setTitle("Stack application");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(null);
		setSize(488,309);
		setVisible(false);

		JButtonPush.setText("Push");
		getContentPane().add(JButtonPush);
		JButtonPush.setBounds(36,48,110,27);

		JButtonPop.setText("Pop");
		getContentPane().add(JButtonPop);
		JButtonPop.setBounds(36,96,110,27);

		JButtonUndo.setText("Undo");
		getContentPane().add(JButtonUndo);
		JButtonUndo.setBounds(36,144,110,27);

		JButtonRedo.setText("Redo");
		getContentPane().add(JButtonRedo);
		JButtonRedo.setBounds(36,192,110,27);

		JScrollPane scrollPane = new JScrollPane(JList1);
		JList1.setListData(stack.getStackVector());
		scrollPane.setBounds(216,36,162,203);

		getContentPane().add(scrollPane);

		SymWindow aSymWindow = new SymWindow();
		this.addWindowListener(aSymWindow);
		SymAction lSymAction = new SymAction();

		JButtonPush.addActionListener(lSymAction);
		JButtonPop.addActionListener(lSymAction);
		JButtonUndo.addActionListener(lSymAction);
		JButtonRedo.addActionListener(lSymAction);

    commandManager.addObserver(this);
	}

	static public void main(String args[])
	{
		try {
		    // Add the following code if you want the Look and Feel
		    // to be set to the Look and Feel of the native system.
		  
		    try {
		        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		    } 
		    catch (Exception e) { 
		    }

			//Create a new instance of our application's frame, and make it visible.
			(new Lab4()).setVisible(true);
		} 
		catch (Throwable t) {
			t.printStackTrace();
			//Ensure the application exits with an error condition.
			System.exit(1);
		}
	}

	JButton JButtonPush = new JButton();
	JButton JButtonPop = new JButton();
	JButton JButtonUndo = new JButton();
	JButton JButtonRedo = new JButton();
	JList JList1 = new JList();


	void exitApplication()
	{
		try {
		    	this.setVisible(false);    // hide the Frame
		    	this.dispose();            // free the system resources
		    	System.exit(0);            // close the application
		} catch (Exception e) {
		}
	}

	class SymWindow extends WindowAdapter
	{
		public void windowClosing(WindowEvent event)
		{
			Object object = event.getSource();
			if (object == Lab4.this)
				JFrame1_windowClosing(event);
		}
	}

	void JFrame1_windowClosing(WindowEvent event)
	{
		// to do: code goes here.
			 
		JFrame1_windowClosing_Interaction1(event);
	}

	void JFrame1_windowClosing_Interaction1(WindowEvent event) {
		try {
			this.exitApplication();
		} catch (Exception e) {
		}
	}

	class SymAction implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			Object object = event.getSource();
			if (object == JButtonPush)
				JButtonPush_actionPerformed(event);
			else if (object == JButtonPop)
				JButtonPop_actionPerformed(event);
			else if (object == JButtonUndo)
				JButtonUndo_actionPerformed(event);
			else if (object == JButtonRedo)
				JButtonRedo_actionPerformed(event);
			
		}
	}

	void JButtonPush_actionPerformed(ActionEvent event)
	{
	    pushstring= "";
	    PushDialog  dialog = new PushDialog(this); //ask the user what to push
	    dialog.setVisible(true);
	    if (!pushstring.equals("")){ // after the dialog is closed, 
		                              // pushstring contains the user input
			commandManager.submit(new PushCommand(stack, pushstring));
		}
		JList1.setListData(stack.getStackVector());  // refresh the JList
		this.repaint();
		//update(commandManager);
	}

	void JButtonPop_actionPerformed(ActionEvent event)
	{
		//new PopCommand(stack).execute();
		commandManager.submit(new PopCommand(stack));
		JList1.setListData(stack.getStackVector()); // refresh the JList
		this.repaint();
		//update(commandManager);
	}

	void JButtonUndo_actionPerformed(ActionEvent event)
	{
		commandManager.undo();
		JList1.setListData(stack.getStackVector());
		this.repaint();
		//update(commandManager);
	}

	void JButtonRedo_actionPerformed(ActionEvent event)
	{
		commandManager.redo();
		JList1.setListData(stack.getStackVector());
		this.repaint();
		//update(commandManager);
			 
	}
	
	public void setPushString (String string){
	    pushstring = string;
	}

	public void update(CommandManager commandManager_){
    System.out.println("Updating observer.");
    //repaint();
		CommandHistory history = commandManager_.commandHistory();
		// logic for undo and redo buttons

		if (history.listPointer()>=0) {
			JButtonUndo.setEnabled(true);
    } else {
			JButtonUndo.setEnabled(false);
    }
		if (history.listSize() - history.listPointer()>1) {
			JButtonRedo.setEnabled(true);
    } else {
			this.JButtonRedo.setEnabled(false);
      System.out.println("JButtonRedo => " + JButtonRedo.isEnabled());
    }
	}

	private CommandManager commandManager = new CommandManager();


}
