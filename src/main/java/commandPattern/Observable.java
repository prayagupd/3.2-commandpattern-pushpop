package commandPattern;

/**
 * Created by prayagupd
 * on 4/8/15.
 */
public interface Observable {
  public void addObserver(Observer observer);
  public void notifyObservers();
}
