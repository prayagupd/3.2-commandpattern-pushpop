package commandPattern.command;

import java.util.*;
/**
 * @author  prayagupd
 * @date    04-08-2015
 */

public class CommandHistory {
    private Vector historyList;
    private int listPointerIndex;
    
    public CommandHistory(){
        historyList = new Vector();
        listPointerIndex =-1;
    }
    
    public int addCommand(Command command){
        historyList.addElement(command);
        ++listPointerIndex;
        return listPointerIndex;
    }
    
    public int undo (){
        if (listPointerIndex >= 0){
            Command command = (Command) historyList.elementAt(listPointerIndex);
            command.undoExecute();
            listPointerIndex--;
        }
        return listPointerIndex;
    }
    
    public int redo (){
        if (listPointerIndex >= -1){
            listPointerIndex++;
            Command command = (Command) historyList.elementAt(listPointerIndex);
            command.execute();
        }
        return listPointerIndex;
    }
    
    public int listSize(){
        return historyList.size();
    }
    public int listPointer(){
        return listPointerIndex;
    }
    
}