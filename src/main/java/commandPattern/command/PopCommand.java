package commandPattern.command;

import commandPattern.VStack;

/**
 * @author  prayagupd
 * @date    04-07-2015
 */

public class PopCommand implements Command {
    private VStack stack;
    private Object poppedValue;

    public PopCommand( VStack stack){
        this.stack =stack;
    }
    
    public void execute(){
        poppedValue = stack.pop();
    }

    public void undoExecute() {
        stack.push(poppedValue);
    }
}