package commandPattern.command;

import commandPattern.VStack;

/**
 * @author  prayagupd
 * @date    04-07-2015
 */

public class PushCommand implements Command {
    private VStack stack;
    private String pushString;
    
    public PushCommand( VStack stack, String string){
        this.stack =stack;
        pushString =string;
    }
    
    public void execute(){
        stack.push(pushString);
    }

    public void undoExecute() {
        stack.pop();
    }
}